use axum::{routing::post, Router};

mod request_route;
use request_route::request_route;

const ADDR: &str = "0.0.0.0:3000";

#[tokio::main]
async fn main() {
	let app = Router::new().route("/", post(request_route));

	println!("Binding app at {}, should be up in a few seconds...", ADDR);

	axum::Server::bind(&ADDR.parse().unwrap())
		.serve(app.into_make_service())
		.await
		.unwrap();
}
