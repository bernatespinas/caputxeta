use std::{fs::File, io::Write, process::Command};

use axum::{
	body::StreamBody,
	http::{header, HeaderMap, HeaderValue},
	response::IntoResponse,
	Form,
};
use serde::Deserialize;
use tokio_util::io::ReaderStream;

const SCRIPT_FILE_NAME: &str = "tests/request-route.spec.ts";
const SCREENSHOT_FILE_NAME: &str = "screenshot.png";

#[derive(Deserialize)]
pub struct RouteRequest {
	from: String,
	to: String,
}

pub async fn request_route(Form(route_request): Form<RouteRequest>) -> impl IntoResponse {
	generate_browser_automation_script(&route_request.from, &route_request.to).unwrap();

	execute_script_and_take_screenshot().unwrap();

	send_screenshot().await
}

fn generate_browser_automation_script(from: &str, to: &str) -> Result<(), String> {
	let script = generate_playwright_script(from, to);

	println!("{}", script);

	write!(File::create(SCRIPT_FILE_NAME).unwrap(), "{}", script)
		.map_err(|err| format!("Could not open screenshot: {err}"))
}

fn execute_script_and_take_screenshot() -> Result<(), String> {
	Command::new("npx")
		.args(["playwright", "test", SCRIPT_FILE_NAME, "--project=chromium"])
		.spawn()
		.map_err(|err| format!("Playwright script failed: {err}"))?
		.wait()
		.unwrap();

	Ok(())
}

async fn send_screenshot() -> impl IntoResponse {
	let file = tokio::fs::File::open(SCREENSHOT_FILE_NAME)
		.await
		.expect("could not open screenshot");

	let stream = ReaderStream::new(file);
	let body = StreamBody::new(stream);

	let mut headers = HeaderMap::new();
	headers.insert(header::CONTENT_TYPE, HeaderValue::from_static("image/png"));

	(headers, body)
}

fn generate_playwright_script(from: &str, to: &str) -> String {
	format!(
		"import {{ test, expect }} from '@playwright/test';

test.describe('Request route', () => {{
	test('Request route to Google Maps', async ({{ page }}) => {{
	  await page.goto('https://www.google.com/maps/?hl=en');

	  await page.getByRole('button', {{ name: 'Accept all' }}).click();

	  await page.getByRole('textbox', {{ id: 'searchboxinput' }}).fill('{to}');

	  await page.locator('[aria-label=Directions]').click();

	  await page.getByPlaceholder('Choose starting point').fill('{from}');

	  await page.getByPlaceholder('Choose starting point').press('Enter');

	  // This works when executing caputxeta in a fedora:37 toolbox, but not when running the podman image... I'll replace it with a timeout...
	  // await page.getByText('Loading...').first().waitFor({{state: 'hidden'}});

	  await page.waitForFunction(() => new Promise(resolve => setTimeout(resolve, 3000)));

	  // To debug in case that the timeout ends up not being reliable.
	  await page.screenshot({{ path: 'after_timeout.png' }});

	  await page.locator('[role=main]').waitFor({{state: 'visible'}});

	  await page.locator('[aria-label=Transit]').click();

	  await page.waitForFunction(() => new Promise(resolve => setTimeout(resolve, 3000)));

	  await page.screenshot({{ path: '{SCREENSHOT_FILE_NAME}' }});
	}});
}});"
	)
}
