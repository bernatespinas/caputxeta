FROM fedora:37

WORKDIR /caputxeta

RUN dnf install -y npm $(dnf repoquery --requires --resolve chromium | grep x86_64) && \
    dnf clean all && \
    npm install -g playwright && \
    npm install -D @playwright/test && \
    mkdir tests && \
    npm init -y

COPY playwright.config.ts playwright.config.ts

EXPOSE 3000

CMD ["./caputxeta"]

COPY target/debug/caputxeta caputxeta
